---

## Configuration Server

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. For Spring Cloud to be able to encrypt and decrypt properties you will need to add the full-strength JCE to your JVM. 
   The Zip file in docs must be decompressed under <java-home>/lib/security  
2. The art of encryption must be configured in application.properties. encrypt.key attribute is responsible for type of
   encryption. In this project we use symmetric encryption
3. Userid and password can be encrypted. First we start the application and with Curl or Postman (pictured added already) 
   we are able to encrypt any test. There is an example *curl localhost:8888/encrypt -d password_of_git_repository* 

---

